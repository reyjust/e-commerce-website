<!-- IMPORTS -->
<?php 
require_once('modules/getLoggedUser.php');
$userStatus = getLoggedUser();

require_once('modules/head.php');
require_once('modules/navigation.php');
require_once('modules/divider.php');
require_once('modules/footer.php');
require_once('modules/import.php');
require_once('queries/shop-queries.php');
?>

<!DOCTYPE html>
<?php outputHeader('Home');
$products = getShop(); ?>
<body>
  <?php
    outputNavigation('Shop', $userStatus);
   ?>
  <section class="section-coloured text-dark">
    <br />
    <div class="container col-lg-11 large-card">
        <h1 class="text-center title">Shop</h1>
        <?php echo outputDivider('store', '4','dark'); ?>
        <div class="container">
          <div class="row">
            <div class="col-lg">
              <input class="form-control" type="text" id="searchProduct" placeholder="Search for socks..">
            </div>
            <div class="col-lg-2">
              <select class="form-control" name="filter" id="searchFilter">
                <option value="price">price</option>
                <option value="name">name</option>
              </select>
            </div>
            <div class="col-lg-2">
              <select class="form-control" name="order" id="searchOrder">
                <option value="1">Ascending</option>
                <option value="-1">Descending</option>
              </select>
            </div>
            </div>
        </div>
        <br /><br />
        <!-- Shop -->
        <div id="shop" class="row justify-content-center">
        </div>

        <button class="product-btn" id="loadMore" style="width: 100px;">Load More</button>
        <br />

    </div>
  </section>
  <?php outputImport(); ?>

  <script type="module" src="./js/addToCart.js"></script>
  <script type="module" src="./js/shop.js"></script>
    
</body>

</html>
<?php outputFooter('white');  ?>