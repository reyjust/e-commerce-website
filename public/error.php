<!--
Comment of different section are display inside php modules only to avoid duplication.
You can consult them in modules files or after html injection on the developer console.
-->

<!-- IMPORTS -->
<?php

require_once('modules/footer.php');
require_once('modules/import.php');
require_once('modules/divider.php');
require_once('modules/head.php');
?>

<!DOCTYPE html>
<?php outputHeader('Home');?>

<body>
    <!-- Landing Page -->
    <section class="section-coloured text-dark text-center">
            <div class="large-card col-lg-10 container">
            <?php echo outputDivider('exclamation', '4', 'dark'); ?>
            <div class="container align-middle col-lg-12">
                <h1 class="title text-uppercase mb-0">Access Forbidden!</h1>
                <br />
                <h3>Go back to Homepage</3>
                <br />
                <div class="text-center mt-4">
                    <a class="btn btn-xl btn-outline-dark" href="/index.php">
                        Click here
                    </a>
                </div>
            </div>
        </div>


        </div>
    </section>
    <?php outputImport(); ?>
    
</body>
<?php outputFooter('white');  ?>