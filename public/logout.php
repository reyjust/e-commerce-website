<?php

require_once('modules/getLoggedUser.php'); 
$userStatus = getLoggedUser();

session_start();
session_destroy();
header('Location: /index.php');
?>