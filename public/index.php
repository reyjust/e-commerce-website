<!--
Comment of different section are display inside php modules only to avoid duplication.
You can consult them in modules files or after html injection on the developer console.
-->

<!-- IMPORTS -->
<?php

define('SERVER_ROOT', __FILE__);

require_once('modules/getLoggedUser.php'); 
$userStatus = getLoggedUser();

require_once('modules/head.php');
require_once('modules/navigation.php');
require_once('modules/footer.php');
require_once('modules/import.php');?>

<!DOCTYPE html>
<?php 
    outputHeader('Home');
?>

<body>
    <?php
        outputNavigation('Home', $userStatus);
    ?>
    <!-- Landing Page -->
    <section class="section-coloured text-white text-center">
        <div class="row">
            <div class="container align-middle col-lg-6">
                <h1 class="title text-uppercase mb-0">Shop Any Left Socks you want!</h1>
                <div class="text-center mt-4">
                    <a class="btn btn-xl btn-outline-light" href="/shop.php">
                        Shop Now
                    </a>
                </div>
            </div>
            <!-- Image -->
            <div class="container align-items-center col-lg-4">
                <img src="/assets/img/sock.png" class="" id="MainImage" width="200px" alt="Responsive image">
            </div>
        </div>


        </div>
    </section>
  <section class="section-white text-dark">
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <h2 style="font-size: 500;">Every type of socks, all at the same place.</h2><br/>
            <p>Wondering where have you lost the left side of your pair of socks?<br>
                Don't Worry, We have a solution!<br>
                Here at SockStar.com, We have every single type of socks, in every colors, in every sizes for all your needs!<br>
                Never get angry for not finding the other sock!Wear them like a Rock Star, or a Sock Star!<br>
                It's not like socks doesn't have a specific side...
            </p>
        </div>
    </div>
  </section>
    <?php
    outputImport(); ?>
    <script type="module" src="./js/isLoggedIn.js"></script>
    
</body>
<?php outputFooter('coloured');  ?>