<!-- IMPORTS -->
<?php
//session_start must be the first thing!
require_once('modules/getLoggedUser.php');
$userStatus = getLoggedUser();

require_once('modules/navigation.php');
require_once('modules/divider.php');
require_once('modules/footer.php');
require_once('modules/import.php');
require_once('modules/head.php');
?>

<!DOCTYPE html>
<?php
outputHeader('Home');  ?>

<body>
  <?php
    outputNavigation('My Account', $userStatus);
   ?>
  <section class="section-coloured text-dark">
    <br />
    <div class="container col-lg-11 large-card">
      <?php echo outputDivider('user', '4', 'dark'); ?>
      <div class="row justify-content-center">
        <div class="col-lg">
            <img src="./assets/img/avatars/guy.png" alt="men" class="avatar" id="main-avatar">
            <br />
            <h1 id="username">Username</h1>
            <h4 id="orders">0 Orders</h4>
        </div>
        <div class="col-lg-6" style="border 1px solid black">
          <div class="container col-lg-6   text-left">
            <div class="row">
              <h3 class="title">General Informations</h3>
              <button id="updateModalBtn" class='update' data-bs-toggle="modal" data-bs-target="#updateModal"><i class='text-center fas fa-pen fa-2x'></i></button>
            </div>
            <br />
            <div class="row">
              <div class="col">
                <p><b>Id:</b></p>
              </div>
              <div class="col">
                <p id="profile_id"><b>Value</b></p>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <p><b>Email:</b></p>
              </div>
              <div class="col">
                <p id="profile_email">Value</p>
              </div>
            </div>

            <div class="row">
              <div class="col">
                <p><b>Name:</b></p>
              </div>
              <div class="col">
                <p id="profile_name">Value</p>
              </div>
            </div>

            <div class="row">
              <div class="col">
                <p><b>Surname:</b></p>
              </div>
              <div class="col">
                <p id="profile_surname">Value</p>
              </div>
            </div>

            <div class="row">
              <div class="col">
                <p><b>Phone Number:</b></p>
              </div>
              <div class="col">
                <p id="profile_phone_number">Value</p>
              </div>
            </div>

            <div class="row">
              <div class="col">
                <p><b>Date Joined:</b></p>
              </div>
              <div class="col">
                <p id="profile_date">Value</p>
              </div>
            </div>

          </div>

        </div>
      </div>
        <?php echo outputDivider('history', '3', 'dark'); ?>
      <div class="custom-table">
        <table>
          <thead>
            <tr>
              <th>Order Id</th>
              <th>Date</th>
              <th>Receipt</th>
            </tr>
          </thead>
          <tbody  id="order-customer-table">
          </tbody>
        </table>
      </div> 
      <!-- Modal -->
  <div class="modal fade" id="updateModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update My Informations</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
        </div>

        <div class="modal-body">
          <label for="update_id">User Id</label>
          <input id="update_id" class="form-control" type="text" disabled class="form-control">
          <label for="update_email">Email</label>
          <input id="update_email" class="form-control" type="text" placeholder="Username" required/>
          <br />
          <label for="update_name">Name</label>
          <input id="update_name" class="form-control" type="text" placeholder="Username" required/>
          <br />
          <label for="update_surname">Surname</label>
          <input id="update_surname" class="form-control" type="text" placeholder="Username" required/>
          <br />
          <label for="update_phone_number">Phone Number</label>
          <input id="update_phone_number" class="form-control" type="number" placeholder="Username" required/>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button id="updateBtn" type="button" class="btn btn-primary" data-bs-dismiss="modal">Update</button>
        </div>

      </div>
    </div>
  </div>
  <div class="modal fade" id="receiptModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Receipt Summary</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
        </div>

        <div class="modal-body">
          <label for="receipt_id">Order Id</label>
          <input id="receipt_id" class="form-control" type="text" disabled class="form-control">
          <div id="receiptBody">
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
    </div>
  </section>
  
  <?php
  outputImport(); ?>
  <script type="module" src="./js/isLoggedIn.js"></script>
  <script type="module" src="./js/account.js"></script>
    
</body>

</html>
<?php
outputFooter('white');  ?>