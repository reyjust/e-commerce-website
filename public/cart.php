<!-- IMPORTS -->
<?php

//session_start must be the first thing!
require_once('modules/getLoggedUser.php');
$userStatus = getLoggedUser();

require_once('modules/head.php');
require_once('modules/navigation.php');
require_once('modules/divider.php');
require_once('modules/footer.php');
require_once('modules/import.php');
?>

<!DOCTYPE html>
<?php
outputHeader('Cart');  ?>

<body>
  <?php
      outputNavigation('Cart', $userStatus);
  ?>
  <section class="section-coloured text-dark">
    <br />
    <div class="container col-lg-11 large-card">
      <?php echo outputDivider('shopping-cart', '4', 'dark'); ?>
      <div id="custom-table" class="custom-table">
        <table>
          <thead>
            <tr>
              <th>Product Name</th>
              <th>Quantity</th>
              <th>Price</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody  id="product-cart-table">
          </tbody>
        </table>
      </div>
      <br />
      <button id="checkout" class="btn btn-xl btn-outline-dark" data-bs-toggle="modal" data-bs-target="#okModal">Checkout</button>
          <!-- Modal -->
        <div class="modal fade text-center" id="okModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog text-center">
              <div class="modal-content text-center">
                <div class="modal-header text-center">
                  <h5 class="modal-title text-center" id="exampleModalLabel">Confirm Order</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
                </div>
                <div class="modal-body" id="modal-body">
                  <p id="modalText"></p>
                </div>
                <div class="modal-footer">
                  <button id="closeCheckout" type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
    </div>
  </section>
  <?php
  outputImport(); ?>
  <script type="module" src="./js/isLoggedIn.js"></script>
  <script type="module" src="./js/cart.js"></script>
    
</body>

</html>
<?php
outputFooter('white');  ?>