<!-- IMPORTS -->
<?php 

require_once('modules/getLoggedUser.php'); 
$userStatus = getLoggedUser();

checkRole($userStatus);

require_once('modules/head.php');
require_once('modules/cmsNavigation.php');
require_once('modules/divider.php');
require_once('modules/cmsFooter.php');
require_once('modules/import.php');
?>

<!DOCTYPE html>
<?php outputHeader('Customers');  ?>

  <?php outputCmsNavigation('Customers');  ?>
    <div class="col-10 offset-2" id="main">
      <body>
      <section class="section-coloured text-dark">
        <div class="large-card">
          <center><h1>Customers</h1></center>
          <?php echo outputDivider('user', '4', 'dark'); ?>
          <div class="custom-table">
            <table>
              <thead>
                <tr>
                  <th>Customer Id</th>
                  <th>Email</th>
                  <th>Number of Orders</th>
                  <th>Date Enrolled</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody  id="customer-table">
              </tbody>
            </table>
          </div> 
          </br>
          <button class="btn btn-xl btn-outline-dark" data-bs-toggle="modal" data-bs-target="#addModal">Add</button>

          <!-- Modal -->
          <div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Add Customer</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
                </div>
                <div class="modal-body">
                  <label for="username">Username</label>
                  <input id="username" type="text" class="form-control" placeholder="Username" required>
                  <br />
                  <label for="name">Name</label>
                  <input id="name" type="text" class="form-control" placeholder="Name" required>
                  <br />
                  <label for="surname">Surname</label>
                  <input id="surname" type="text" class="form-control" placeholder="Surname" required>
                  <br />
                  <label for="email">Email Address</label>
                  <input id="email" type="email" class="form-control" placeholder="Email" required>
                  <br />
                  <label for="password">Password</label>
                  <input id="password" type="text" class="form-control" placeholder="Password" required>
                  <br />
                  <label for="phone_number">Phone Number</label>
                  <input id="phone_number" type="number" class="form-control" placeholder="Phone number" required>
                  <br />
                  <label for="phone_number">Gender</label>
                  <br/>
                  <center>
                    <label for="male">Male</label>
                    <input id="male" name="gender" type="radio" value="male">
                    <label for="female">Female</label>
                    <input id="female" name="gender" type="radio" value="female">
                  </center>
                  <br />
                  <label for="phone_number">Role</label>
                  <br/>
                  <center>
                    <label for="admin">Admin</label>
                    <input id="admin" name="role" type="radio" value="admin">
                    <label for="customer">Customer</label>
                    <input id="customer" name="role" type="radio" value="customer">
                  </center>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                  <button id="addBtn" type="button" class="btn btn-primary" data-bs-dismiss="modal">Save Customer</button>
                </div>
              </div>
            </div>
          </div>

          <!-- Modal -->
          <div class="modal fade" id="updateModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Update Customer</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
                </div>
                <div class="modal-body">
                  <label for="update_id">Customer Id</label>
                  <input id="update_id" type="text" disabled class="form-control">
                  <label for="update_username">Username</label>
                  <input id="update_username" type="text" class="form-control" placeholder="Username" required>
                  <br />
                  <label for="update_name">Name</label>
                  <input id="update_name" type="text" class="form-control" placeholder="Name" required>
                  <br />
                  <label for="update_surname">Surname</label>
                  <input id="update_surname" type="text" class="form-control" placeholder="Surname" required>
                  <br />
                  <label for="update_email">Email Address</label>
                  <input id="update_email" type="email" class="form-control" placeholder="Email" required>
                  <br />
                  <label for="update_password">Password</label>
                  <input id="update_password" type="text" class="form-control" placeholder="Password" required>
                  <br />
                  <label for="update_phone_number">Phone Number</label>
                  <input id="update_phone_number" type="number" class="form-control" placeholder="Phone number" required>
                  <br />
                  <label for="gender">Gender</label>
                  <br/>
                  <center>
                    <label for="update_male">Male</label>
                    <input id="update_male" name="gender" type="radio" value="male">
                    <label for="update_female">Female</label>
                    <input id="update_female" name="gender" type="radio" value="female">
                  </center>
                  <br />
                  <label for="role">Role</label>
                  <br/>
                  <center>
                    <label for="update_admin">Admin</label>
                    <input id="update_admin" name="role" type="radio" value="admin">
                    <label for="update_customer">Customer</label>
                    <input id="update_customer" name="role" type="radio" value="customer">
                  </center>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                  <button id="updateBtn" type="button" class="btn btn-primary" data-bs-dismiss="modal">Update Customer</button>
                </div>
              </div>
            </div>
          </div>

        </div>
      </section>
      <?php outputCmsFooter();  ?>
      <?php outputImport(); ?>
      <script type="module" src="./js/customer_mng.js"></script>
      </body>
    </div></div></div>
</html>