<!-- IMPORTS -->
<?php

require_once('modules/getLoggedUser.php'); 
$userStatus = getLoggedUser();
checkRole($userStatus);

require_once('modules/head.php');
require_once('modules/cmsNavigation.php');
require_once('modules/divider.php');
require_once('modules/cmsFooter.php');
require_once('modules/import.php');
?>

<!DOCTYPE html>
<?php outputHeader('Order');  ?>

  <?php outputCmsNavigation('Order');  ?>
    <div class="col-10 offset-2" id="main">
      <body>
      <section class="section-coloured text-dark">
        <div class="large-card">
          <center><h1>Orders</h1></center>
          <?php echo outputDivider('dollar-sign', '4', 'dark'); ?>
          <div class="custom-table">
            <table>
              <thead>
                <tr>
                  <th>Order Id</th>
                  <th>Customer Id</th>
                  <th>Date</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody  id="order-table">
              </tbody>
            </table>
          </div> 
          </br>
        </div>
      </section>
      <?php outputCmsFooter();  ?>
      <?php outputImport(); ?>
      <script type="module" src="./js/order_mng.js"></script>
      </body>
    </div></div></div>
</html>