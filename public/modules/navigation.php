<?php

/**
 * Generate the HTML for the navigation Bar
 *
 * @param String $title Tab name
 * @param String $userStatus  status of user
 * 
 * @return Null
 */
function outputNavigation($title, $userStatus = array("status" => "Not logged In"))
{
        if ($userStatus["status"] == "Logged In") {
            $linkNames = array("Home", "Shop","Account","Cart", "Log Out");
            $linkAddresses = array("index.php", "shop.php" ,"account.php","cart.php", "logout.php");
            $className = array("index", "shop" ,"account","cart", "logout");

            if ($userStatus["logged_role"] == "admin") {
                array_push($linkNames, "CMS");
                array_push($linkAddresses, "product_mng.php");
                array_push($className, "cms");
            }
        } else {
            $linkNames = array("Home", "Shop", "Log In", "Sign Up");
            $linkAddresses = array("index.php", "shop.php", "login.php", "signup.php");
            $className = array("index", "shop", "login", "signup");
        }


    $navigation = '<!-- Navigation-->
    <nav class="navbar navbar-expand-lg text-uppercase fixed-top navbar-custom" id="mainNav">
        <div class="container">
            <a class="navbar-brand navbar-brand-custom text-dark" href="/index.php">SockStar.com</a>

            <button class="navbar-toggler font-weight-bold bg-primary text-white rounded" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars"></i>
            </button>

            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul id="navbar" class="navbar-nav ml-auto">';

    echo $navigation;
    //Output navigation
    for ($x = 0; $x < count($linkNames); $x++) {
        echo '<li class="nav-item mx-0 mx-lg-1 ' . $className[$x] . '"><a ';
        if ($linkNames[$x] == $title) {
            echo 'class="nav-link py-3 px-0 px-lg-3 rounded active"';
        } else {
            echo 'class="nav-link py-3 px-0 px-lg-3 rounded"';
        }
        echo ' href="' . $linkAddresses[$x] . '">' . $linkNames[$x] . '</a></li>';
    }

    echo "</ul>
            </div>
        </div>
    </nav>';";
}
