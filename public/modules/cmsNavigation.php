<?php
include_once('check.php');

//Prevent direct url access
check(realpath(__FILE__), $_SERVER['SCRIPT_FILENAME']);
/**
 * Generate the HTML for the Cms navigation Bar
 *
 * @param String $title Cms Tab name
 * 
 * @return Null
 */
function outputCmsNavigation($title)
{
    $linkNames = array("Products", "Orders", "Customers", "Exit");
    $linkAddresses = array("product_mng.php", "order_mng.php" ,"customer_mng.php", "index.php");
    $className = array("product_mng", "order_mng" ,"customer_mng", "home");

    $navigation ='<!-- Navigation-->
    <div class="container-fluid">
    <div class="row">
        <div class="col-2 px-1 bg-white position-fixed" id="mainCmsNav">
        <br />
        <a class="flex-column navbar-brand navbar-brand-custom text-dark" href="/index.php">SockStar.com</a>
        <div class="nav flex-column flex-nowrap vh-100 overflow-auto text-dark p-2 cmsNavContent">';

    echo $navigation;

    //Output navigation
    for ($x = 0; $x < count($linkNames); $x++) {
        if ($linkNames[$x] == $title) {
            echo '<a href="'. $linkAddresses[$x] .'" class="nav-link text-decoration-none active '. $className[$x] .'">'. $linkNames[$x] .'</a>';
        } else {
            echo '<a href="'. $linkAddresses[$x] .'" class="nav-link text-decoration-none'. $className[$x] .'">'. $linkNames[$x] .'</a>';
        }
    }

        echo '</div></div>';
}
