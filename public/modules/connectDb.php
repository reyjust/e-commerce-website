<?php

require_once dirname(dirname(__DIR__)) . '/vendor/autoload.php';
include_once('check.php');

//Prevent direct url access
check(realpath(__FILE__), $_SERVER['SCRIPT_FILENAME']);

/**
 * Connecting to MongoDB parameters
 * 
 * @return status  connecting status to database object  
 */

function connectDb() {
    try {
        $client = new MongoDB\Client('mongodb://127.0.0.1:27017/?compressors=disabled&gssapiServiceName=mongodb');
        $db = $client->ecommerce_db;
        $status = array("Connected", $db, $client);
    } catch (MongoDB\Client\Exception $e) {
        $status = 'Connection to Database failed: ' . $e->getMessage() . '\n';
        $status = array($status, NULL);
    }

    return $status;
}
?>