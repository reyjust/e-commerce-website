<?php
include_once('check.php');

//Prevent direct url access
check(realpath(__FILE__), $_SERVER['SCRIPT_FILENAME']);
/**
 * Generate the HTML for a the page Header
 *
 * @param String $title Name of the Tab
 * 
 * @return Null
 */
function outputHeader($title)
{
  $header = '<html><head>
      <link rel="icon" href="../assets/img/sock.ico" sizes="16x16">
      <title>' . $title . '</title>
      <!-- Icons -->
      <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
      <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
      <!-- Importing Fonts -->
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Outfit:wght@300;500&display=swap" rel="stylesheet">

      <!-- CSS -->
      <link href="css/styles.css" rel="stylesheet" />

      <!-- Bootstrap -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>';

  echo $header;
}
