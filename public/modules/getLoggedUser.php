<?php
include_once('check.php');

//Prevent direct url access
check(realpath(__FILE__), $_SERVER['SCRIPT_FILENAME']);
require_once(dirname(__FILE__).'/connectDb.php');

function getLoggedUser() {
    //If Session Started
    if (session_status() === PHP_SESSION_NONE) {
        session_start();
    }
    //If logged In value exist in php session
    if (isset($_SESSION["logged_email"]) && isset($_SESSION["logged_password"])) {
        $mongo = connectDb();
        $db = $mongo[1];
        
        $collection = $db->customers;
        $cursor = $collection->findOne( array("email" => $_SESSION["logged_email"]) );
        //If values doesn't meet the databases infos + give user role.
        if ($_SESSION["logged_email"] == $cursor['email'] && $_SESSION["logged_password"] == $cursor['password'] ) {
            return 
                array(
                    "status" => "Logged In",
                    "logged_email" => $_SESSION["logged_email"],
                    "logged_password" => $_SESSION["logged_password"],
                    "logged_role" => $cursor["role"],
                    "logged_id" => $cursor["_id"]
                );
        } else {
            return array(
                "status" => "Error while checking status"
            );
        }
    } else {
        return array(
            "status" => "Not logged In"
            );
    }
    
}
