<?php
include_once('check.php');

//Prevent direct url access
check(realpath(__FILE__), $_SERVER['SCRIPT_FILENAME']);
/**
 * Generate the HTML for the footer
 *
 * @param String $color Colour for the font
 * 
 * @return Null
 */
function outputFooter($color)
{
    $fontColor;
    if ($color == 'white') {
        $fontColor = 'dark';
    } else {
        $fontColor = 'light';
    }
    $footer = '<!-- Footer-->
<footer class="footer section-'. $color .' text-'. $fontColor . '">
    <div class="container">
        <div class="row ">
            <div class="col-lg-6">
                <h4 class="text-uppercase mb-4 title  text-center" style="font-size: 500;">About</h4>
                <p class="lead mb-0 ">
                    This website is intended to recreate a real life E-commerce application .To do so, HTML, CSS, JavaScript and PHP are used.<br />
                    Also Bootstrap have been use for a better implementation of the css and we found inspiration on free Templates.
                    CST2120 Coursework.
                </p>
            </div>
            <div class="col-lg-6 text-center">
                <h4 class="text-uppercase mb-4 title" style="font-size: 500;">Contact Us</h4>
                <p>Address: Chemin Grenier</p>
                <p>Phone Number: +230 6758 6547</p>
                <p>Email: sock@assistance.left</p>
            </div>
        </div>
    </div>
</footer>
<section class="copyright py-4 text-white row">
<div class="container text-start col-lg-5"><small>Made with &#x1F9E1 by Justin, Farhaan and Florrie</small></div>
    <div class="container text-right col-lg-5"><small>A Web Application and Database Coursework</small>
    </div>
</section>
</html>';
    echo $footer;
}
