<?php

//Prevent direct url access to unhautorized files.
function check($path, $script_name) {
    if ( $_SERVER['REQUEST_METHOD']=='GET' && $path == realpath( $script_name ) ) {

        header( 'HTTP/1.0 403 Forbidden', TRUE, 403 );
    
        die( header( 'location: http://' . getenv('HTTP_HOST') . '/error.php' ) );
    
    }
}

//Prevent access for people with not enough privileges
function checkRole($userSessionData) {
    if ( $userSessionData["status"] == "Not logged In" || $userSessionData["status"] ==  "Error while checking status") {

        header( 'HTTP/1.0 403 Forbidden', TRUE, 403 );
    
        die( header( 'location: http://' . getenv('HTTP_HOST') . '/error.php' ) );
    
    } else if ($userSessionData["logged_role"] != "admin") {

        header( 'HTTP/1.0 403 Forbidden', TRUE, 403 );
    
        die( header( 'location: http://' . getenv('HTTP_HOST') . '/error.php' ) );

    }
}

//Prevent direct url access
check(realpath(__FILE__), $_SERVER['SCRIPT_FILENAME']);

?>