<?php
include_once('check.php');

//Prevent direct url access
check(realpath(__FILE__), $_SERVER['SCRIPT_FILENAME']);
/**
 * Generate the HTML for the footer
 *
 * @return Null
 */
function outputCmsFooter()
{
    $footer = '<!-- Footer-->
<footer class="text-light" style="padding: 0">
    <section class="copyright py-4 text-white row">
        <div class="container text-start col-lg-5"><small>Made with &#x1F9E1 by Justin, Farhaan and Florrie</small></div>
        <div class="container text-right col-lg-5"><small>A Web Application and Database Coursework</small>
        </div>
    </section>
</footer>

</html>';
    echo $footer;
}
