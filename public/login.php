<!-- IMPORTS -->
<?php

require_once('modules/getLoggedUser.php'); 
$userStatus = getLoggedUser();

require_once('modules/head.php');
require_once('modules/navigation.php');
require_once('modules/divider.php');
require_once('modules/footer.php');
require_once('modules/import.php');
?>


<!DOCTYPE html>

<!-- Call function Header
  @param String Home Page-->
<?php outputHeader('Home'); ?>

<body>

<!-- Call function Navigation
   @param String Log In Page-->
  <?php
    outputNavigation('Log In');
   ?>
  <section class="section-coloured text-dark">
    <div class="container col-lg-5 ">
      <div class="form">
        <h1 class="text-center title">Log In</h1>
        <?php echo outputDivider('key', '2','dark');?>
        <br />
        <div class="row justify-content-center">
          <div class="col-lg-10">
            <label for="email">Email</label>
            <input id="email" type="text" class="form-control" placeholder="Email" required>
            <br />
            <label for="password">Password</label>
            <input id="password" type="password" class="form-control" placeholder="Password" required>
          </div>
        </div>
        <br />
        <div class="text-center mt-4">
          <button type="submit" id="submit" class="btn btn-lg btn-outline-dark text-center">Log In</button>
        </div>
      </div>
  </section>
  <?php outputImport(); ?>
  <script type="module" src="./js/login.js"></script>
</body>

</html>

<!-- Call function Navigation
   @param String white colour -->
<?php
outputFooter('white');  ?>