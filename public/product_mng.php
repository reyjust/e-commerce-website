<!-- IMPORTS -->
<?php 

require_once('modules/getLoggedUser.php'); 
$userStatus = getLoggedUser();
checkRole($userStatus);

require_once('modules/head.php');
require_once('modules/cmsNavigation.php');
require_once('modules/divider.php');
require_once('modules/cmsFooter.php');
require_once('modules/import.php');
?>

<!DOCTYPE html>
<?php outputHeader('Products');  ?>

  <?php outputCmsNavigation('Products');  ?>
    <div class="col-10 offset-2" id="main">
      <body>
      <section class="section-coloured text-dark">
        <div class="large-card">
          <center><h1>Products</h1></center>
          <?php echo outputDivider('tags', '4', 'dark'); ?>
          <div class="custom-table">
            <table>
              <thead>
                <tr>
                  <th>Product Id</th>
                  <th>Product Name</th>
                  <th>Product Price</th>
                  <th>Product Image</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody  id="product-table">
              </tbody>
            </table>
          </div> 
          </br>
          <button class="btn btn-xl btn-outline-dark" data-bs-toggle="modal" data-bs-target="#addModal">Add</button>
          <!-- <button class="btn btn-xl btn-outline-dark" data-bs-toggle="modal" data-bs-target="#delModal">Delete</button> -->

          <!-- Modal -->
          <div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Add Product</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
                </div>
                <div class="modal-body">
                <label>Product Name</label>
                <input id="p-name" class="form-control" type="text"/>
                <br />
                <label>Product Image</label>
                <input id="p-image" class="form-control" type="text"/>
                <br />
                <label>Product Price</label>
                <input id="p-price" class="form-control" type="number"/>
                <label>Tags (separated with commas)</label>
                <input id="p-tags" class="form-control" type="text"/>
                <br />
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                  <button id="addBtn" type="button" class="btn btn-primary" data-bs-dismiss="modal">Add</button>
                </div>
              </div>
            </div>
          </div>

          <!-- Modal -->
          <div class="modal fade" id="updateModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Update Product</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
                </div>
                <div class="modal-body">
                  <label for="update_id">Product Id</label>
                  <input id="update_id" class="form-control" type="text" disabled class="form-control">
                <label>Product Name</label>
                <input id="update_name" class="form-control" type="text"/>
                <br />
                <label>Product Image</label>
                <input id="update_image" class="form-control" type="text"/>
                <br />
                <label>Product Price</label>
                <input id="update_price" class="form-control" type="number"/>
                <br />
                <label>Tags (separated with commas)</label>
                <input id="update_tags" class="form-control" type="text"/>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                  <button id="updateBtn" type="button" class="btn btn-primary" data-bs-dismiss="modal">Update</button>
                </div>
              </div>
            </div>
          </div>

        </div>
      </section>
      <?php outputCmsFooter();  ?>
      <?php outputImport(); ?>
      <script type="module" src="./js/product_cms.js"></script>
      </body>
    </div></div></div>
</html>
