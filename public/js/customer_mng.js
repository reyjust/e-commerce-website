import { customerMngRow } from "./table_Utilities/customerMngRow.js";
import { emptyTable } from "./table_Utilities/emptyTable.js";
import { getKeyByValue } from "./getKeyByValue.js";

document.addEventListener('DOMContentLoaded', (event) => {
    function getCustomers() {
        return $.ajax({
            url:"queries/customer-cms-queries.php",    //the page containing php script
            type: "post",    //request type,
            // dataType: 'json',
            data: {action: "getCustomers"}
        })
    }

    
     /**
        * Add new data in Customer
        *
        * @param String $user_data 
        *
        * @return ajax method
    */
    function addCustomer(user_data) {
        return $.ajax({
            url:"queries/customer-cms-queries.php",    //the page containing php script
            type: "post",    //request type,
            // dataType: 'json',
            data: {action: "addCustomer", user: user_data}
        })
    }

    /**
        * Delete data in Customer
        *
        * @param String $inputId 
        *
        * @return ajax method
    */
    function delCustomer(inputId) {
        return $.ajax({
            url:"queries/customer-cms-queries.php",    //the page containing php script
            type: "post",    //request type,
            // dataType: 'json',
            data: {action: "delCustomer", userId: inputId}
        })
    }

    /**
        * Update data in Customer
        *
        * @param String $user_data  
        *
        * @return ajax method
    */
    function updateCustomer(user_data) {
        return $.ajax({
            url:"queries/customer-cms-queries.php",    //the page containing php script
            type: "post",    //request type,
            // dataType: 'json',
            data: {action: "updateCustomer", user: user_data}
        })
    }

    /**
        * Get Customer data
        *
        * @param String $cid data on customer and id 
        *
        * @return ajax method
    */
    function getCustomerDataById(cid) {
        return $.ajax({
            url:"queries/customer-cms-queries.php",    //the page containing php script
            type: "post",    //request type,
            // dataType: 'json',
            data: {action: "getCustomerDataById", userId: cid}
        })
    }


    
    /**
        * Delete Customer data
        *
        * @param String $cId
        *
        * @return ajax method
    */
    function deleteCustomer(cId) {
        delCustomer(cId).done(function (result) {
            console.log(result)
            //ReQuery new users
            getCustomers().done(function (result) {
                let customers = JSON.parse(result)
                //Reset Table
                emptyTable("customer-table");
                for (let customer in customers) {
                    customerMngRow("customer-table", customers[customer], customer);
        
                    let updates = document.getElementsByClassName("update")
                    let dels = document.getElementsByClassName("delete")
                
                    updates[customer].setAttribute('data-bs-toggle', 'modal');
                    updates[customer].setAttribute('data-bs-target', '#delModal');

                    dels[customer].addEventListener('click', function () {
                        deleteCustomer(customers[customer].id)
                        }
                    );
                }
            })
        })
    }

    function fillUpdateModal(cid) {
        getCustomerDataById(cid).done(function (result) {
            let user_data = JSON.parse(result);
            console.log(user_data)
            document.getElementById('update_id').value = user_data._id.$oid
            document.getElementById('update_username').value = user_data.username
            document.getElementById('update_name').value = user_data.name
            document.getElementById('update_surname').value = user_data.surname
            document.getElementById('update_email').value = user_data.email
            document.getElementById('update_password').value = user_data.password
            document.getElementById('update_phone_number').value = user_data.phone_number

            if (user_data.gender == "female") {
                document.getElementById('update_female').checked = true
            } else {
                document.getElementById('update_male').checked = true
            }

            if (user_data.role == "admin") {
                document.getElementById('update_admin').checked = true
            } else {
                document.getElementById('update_customer').checked = true
            }
        })
    }

    function customer_cms() {
        getCustomers().done(function (result) {
            let customers = JSON.parse(result)

            for (let customer in customers) {
                customerMngRow("customer-table", customers[customer], customer );
                let updates = document.getElementsByClassName("update")
                let dels = document.getElementsByClassName("delete")
            
                updates[customer].setAttribute('data-bs-toggle', 'modal');
                updates[customer].setAttribute('data-bs-target', '#updateModal');

                updates[customer].addEventListener('click', function () {
                    fillUpdateModal(customers[customer].id)
                    }
                );

                dels[customer].addEventListener('click', function () {
                    deleteCustomer(customers[customer].id)
                    }
                );
            }
        })

        let addBtn = document.getElementById('addBtn');
        let updateBtn = document.getElementById('updateBtn');

        addBtn.addEventListener('click', function () {
            let date = new Date();

            let user_data = {};
            let inputElements = {
                username: document.getElementById('username'),
                name: document.getElementById('name'),
                surname: document.getElementById('surname'),
                email: document.getElementById('email'),
                password: document.getElementById('password'),
                phone_number: document.getElementById('phone_number')
            }

            //getting key by the value
            for (let element in inputElements) {
                user_data[getKeyByValue(inputElements, inputElements[element])] = inputElements[element].value
            }
            user_data['date'] = date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate()

            let checkboxes = {
                male: document.getElementById('male'),
                female : document.getElementById('female'),
                customer: document.getElementById('customer'),
                admin: document.getElementById('admin')
            }
        
            if (checkboxes.male.checked) {
                user_data['gender'] = "male"
            } else if (checkboxes.female.checked) {
                user_data['gender'] = "female" 
            }            

            if (checkboxes.customer.checked) {
                user_data['role'] = "customer"
            } else if (checkboxes.admin.checked) {
                user_data['role'] = "admin" 
            }
            
          
            let isEmpty = false;

            Object.keys(user_data).forEach(function(key){
                if (user_data[key] == "") {
                    console.log("something is empty")
                    isEmpty = true
                }
            })

            if (!isEmpty) {
                //Add a Customer
                addCustomer(user_data).done(function (result) {
                    console.log(result)
                    //ReQuery new users
                    getCustomers().done(function (result) {
                        let customers = JSON.parse(result)
                        //Reset Table
                        emptyTable("customer-table");
                        for (let customer in customers) {
                            customerMngRow("customer-table", customers[customer], customer);
                            let updates = document.getElementsByClassName("update")
                            let dels = document.getElementsByClassName("delete")
                        
                            updates[customer].setAttribute('data-bs-toggle', 'modal');
                            updates[customer].setAttribute('data-bs-target', '#updateModal');

                            updates[customer].addEventListener('click', function () {
                                fillUpdateModal(customers[customer].id)
                                }
                            );
                            dels[customer].addEventListener('click', function () {
                                deleteCustomer(customers[customer].id)
                                }
                            );
                        }
                    })

                })
                //Erasing the inputs
                for (let element in inputElements) {
                    inputElements[element].value = ""
                }
            } else {
                console.log("empty cells");
            }
            for (let checkbox in checkboxes) {
                checkboxes[checkbox].checked = false;
            }
        })

        updateBtn.addEventListener('click', function () {
            let date = new Date();

            let user_data = {};
            let inputElements = {
                id: document.getElementById('update_id'),
                username: document.getElementById('update_username'),
                name: document.getElementById('update_name'),
                surname: document.getElementById('update_surname'),
                email: document.getElementById('update_email'),
                password: document.getElementById('update_password'),
                phone_number: document.getElementById('update_phone_number')
            }

            //getting key by the value
            for (let element in inputElements) {
                user_data[getKeyByValue(inputElements, inputElements[element])] = inputElements[element].value
            }
            user_data['date'] = date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate()

            let checkboxes = {
                male: document.getElementById('update_male'),
                female : document.getElementById('update_female'),
                customer: document.getElementById('update_customer'),
                admin: document.getElementById('update_admin')
            }
        
            if (checkboxes.male.checked) {
                user_data['gender'] = "male"
            } else if (checkboxes.female.checked) {
                user_data['gender'] = "female" 
            }            

            if (checkboxes.customer.checked) {
                user_data['role'] = "customer"
            } else if (checkboxes.admin.checked) {
                user_data['role'] = "admin" 
            }
            
            //Add a Customer
            updateCustomer(user_data).done(function (result) {
                console.log(JSON.parse(result))
                //ReQuery new users
                getCustomers().done(function (result) {
                    let customers = JSON.parse(result)
                    //Reset Table
                    emptyTable("customer-table");
                    for (let customer in customers) {
                        customerMngRow("customer-table", customers[customer], customer);
                        let updates = document.getElementsByClassName("update")
                        let dels = document.getElementsByClassName("delete")
                    
                        updates[customer].setAttribute('data-bs-toggle', 'modal');
                        updates[customer].setAttribute('data-bs-target', '#updateModal');

                        updates[customer].addEventListener('click', function () {
                            fillUpdateModal(customers[customer].id)
                            }
                        );
                        dels[customer].addEventListener('click', function () {
                            deleteCustomer(customers[customer].id)
                            }
                        );
                    }
                })

            })

            //Erasing the inputs
            for (let element in inputElements) {
                inputElements[element].value = ""
            }

            for (let checkbox in checkboxes) {
                checkboxes[checkbox].checked = false;
            }
        })
    }
    customer_cms();
})