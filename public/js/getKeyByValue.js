/**
 * Return a JSON key by its value
 * 
 * @param {Object} object JSON to search from
 * @param {String} value value which we want to find its key
 * 
 * @return {String}  key of the specific value
 */
export function getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
}