import { orderRow } from "./table_Utilities/order-row.js";

//wait for dom to load
document.addEventListener('DOMContentLoaded', (event) => {
  function getUser() {
    return $.ajax({
      url:"queries/account-queries.php",    //the page containing php script
      type: "post",    //request type,
      // dataType: 'json',
      data: {action: "getAccount"}
    })
  }

  function updateProfile(profile_data) {
    return $.ajax({
        url:"queries/account-queries.php",    //the page containing php script
        type: "post",    //request type,
        // dataType: 'json',
        data: {action: "updateProfile", profile: profile_data}
    })
  }
  function getReceipt(oid) {
    return $.ajax({
        url:"queries/account-queries.php",    //the page containing php script
        type: "post",    //request type,
        // dataType: 'json',
        data: {action: "getReceipt", order: oid}
    })
  }
  
  function userOrders(orders) {
    if ( orders.length === 0 ) {
      document.getElementById("order-customer-table").insertAdjacentHTML('beforeend',"<tr><td colspan='3'>No Orders Yet!</td></tr>");
    } else {
      for (let order in orders) {
        orderRow("order-customer-table", orders[order], order);

        let receipts = document.getElementsByClassName("receipts")

        receipts[order].setAttribute('data-bs-toggle', 'modal');
        receipts[order].setAttribute('data-bs-target', '#receiptModal');

        receipts[order].addEventListener('click', function () {
          getReceipt(orders[order].id).done(function (result) {
            let receipt = JSON.parse(result)
            fillReceiptModal(receipt, orders[order].id)
          })
        }
      );
      }
    }
  }

  function fillReceiptModal(receipt, oid) {
    document.getElementById('receipt_id').value = oid

    let receiptBody = document.getElementById('receiptBody');
    receiptBody.innerHTML = "";
    let totalPrice;
    totalPrice = 0;
    for (let item in receipt) {
      totalPrice += parseInt(receipt[item].paid);
      receiptBody.insertAdjacentHTML('beforeend', 
      '<p>' + receipt[item].productName + ' x' + receipt[item].qty + ' = ' + receipt[item].paid + 'RS </p>'
      )
    }

    receiptBody.insertAdjacentHTML('beforeend','<p>Total price RS ' + totalPrice.toString() + '</p>')
  }

  function fillUpdateModal(user_data) {
    document.getElementById('update_id').value = user_data.id
    document.getElementById('update_email').value = user_data.email
    document.getElementById('update_name').value = user_data.name
    document.getElementById('update_surname').value = user_data.surname
    document.getElementById('update_phone_number').value = user_data.phone_number
  }

  function account() {
    /**
     *Get all infos about the logged user.
    */
    //Fetch status

    getUser().done(function (result) {
        let response = JSON.parse(result);

        let userInfo = response.info;
        let orders = response.orders;
        console.log(orders)

        let usernameEl = document.getElementById('username')
        let mainAvatar = document.getElementById("main-avatar")
        let order_num = document.getElementById("orders")
        
        if (userInfo.gender == "male") {
          mainAvatar.src = "../assets/img/avatars/guy.png";
          mainAvatar.alt = "men";
        } else if (userInfo.gender == "female") {
          mainAvatar.src = "../assets/img/avatars/women.png";
          mainAvatar.alt = "women";
        }

        usernameEl.innerHTML = userInfo.username;
        order_num.innerHTML = orders.length + " Orders";

        document.getElementById("profile_id").innerHTML = userInfo._id.$oid
        document.getElementById("profile_email").innerHTML = userInfo.email
        document.getElementById("profile_name").innerHTML = userInfo.name
        document.getElementById("profile_surname").innerHTML = userInfo.surname
        document.getElementById("profile_phone_number").innerHTML = userInfo.phone_number
        document.getElementById("profile_date").innerHTML = userInfo.date

        userOrders(orders);


        document.getElementById('updateBtn').addEventListener('click', function () {
          let gender = document.getElementById("main-avatar").alt

          if (gender == "men") {
            gender = "male"
          } else {
            gender = "female";
          }

          let newProfileData = {
            id: document.getElementById("update_id").value,
            email: document.getElementById("update_email").value,
            name: document.getElementById("update_name").value,
            surname: document.getElementById("update_surname").value,
            phone_number: document.getElementById("update_phone_number").value,
            date: document.getElementById("profile_date").value,
            gender: gender
          }

          updateProfile(newProfileData).done(function (result) {
            let response = JSON.parse(result);

            document.getElementById("profile_email").innerHTML = response.email
            document.getElementById("profile_name").innerHTML = response.name
            document.getElementById("profile_surname").innerHTML = response.surname
            document.getElementById("profile_phone_number").innerHTML = response.phone_number
            document.getElementById("profile_date").innerHTML = response.date
          })

        })

        document.getElementById('updateModalBtn').addEventListener('click', function () {
          let userinfo = {
            id: document.getElementById("profile_id").innerHTML,
            email: document.getElementById("profile_email").innerHTML,
            name: document.getElementById("profile_name").innerHTML,
            surname: document.getElementById("profile_surname").innerHTML,
            phone_number: document.getElementById("profile_phone_number").innerHTML,
          }

          fillUpdateModal(userinfo);
        })

    })
  }

  account()
})