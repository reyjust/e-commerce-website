import { productRow } from "./table_Utilities/product-row.js";
import { emptyTable } from "./table_Utilities/emptyTable.js";
import { getKeyByValue } from "./getKeyByValue.js";


document.addEventListener('DOMContentLoaded', (event) => {

    /**
     * Return a JSON key by its value
     * 
     * @return {Null}}
     */      
    function getProducts() {
        return $.ajax({
            url:"queries/product-cms-queries.php",    //the page containing php script
            type: "post",    //request type,
            // dataType: 'json',
            data: {action: "getProducts"}
        })
    }

    /**
     * Return a JSON key by its value
     * 
     * @param {String} product_data JSON to search from
     * 
     * @return {ajax} ajax promise from product form
     */
    function addProduct(product_data) {
        return $.ajax({
            url:"queries/product-cms-queries.php",    //the page containing php script
            type: "post",    //request type,
            // dataType: 'json',
            data: {action: "addProduct", product: product_data}
        })
    }    

    /**
    * Return a JSON key by its value
    * 
    * @param {String} product_data JSON to search from
    * 
    * @return {ajax} ajax promise from updated product form
    */
    function updateProduct(product_data) {
        return $.ajax({
            url:"queries/product-cms-queries.php",    //the page containing php script
            type: "post",    //request type,
            // dataType: 'json',
            data: {action: "updateProduct", product: product_data}
        })
    }

    /**
     * Return a JSON key by its value
     * 
     * @param {Integer} productId JSON to search from
     * 
     * @return {ajax} ajax promise from productID form
     */
    function delProduct(productId) {
        return $.ajax({
            url:"queries/product-cms-queries.php",    //the page containing php script
            type: "post",    //request type,
            // dataType: 'json',
            data: {action: "delProduct", productId: productId}
        })
    }

    /**
     * Return a JSON key by its value
     * 
     * @param {String} pid JSON to search from
     * 
     * @return {ajax} ajax promise from pid form
     */
    function getProductDataById(pid) {
        console.log(pid)
        return $.ajax({
            url:"queries/product-cms-queries.php",    //the page containing php script
            type: "post",    //request type,
            // dataType: 'json',
            data: {action: "getProductDataById", productId: pid}
        })
    }

    /**
     * Get data for Product by its Id
     * 
     * @param {String} pid data to search from
     * 
     * @return {Null} 
     */
    function fillUpdateModal(pid) {
        getProductDataById(pid).done(function (result) {
            let product_data = JSON.parse(result);
            console.log(product_data)
            document.getElementById('update_id').value = product_data._id.$oid
            document.getElementById('update_name').value = product_data.name
            document.getElementById('update_price').value = product_data.price
            document.getElementById('update_image').value = product_data.image

            let tags = "";
            for (let tag in product_data.tags) {
                tags += product_data.tags[tag] + ','
            }
            tags = tags.slice(0,-1);
            document.getElementById('update_tags').value = tags
        })
    }


     /**
     * Delete data for Product by its Id
     * 
     * @param {String} pid data to search from
     * 
     * @return {Null} 
     */
    function deleteProduct(pId) {
        delProduct(pId).done(function (result) {
            console.log(result)
            //ReQuery new users
            getProducts().done(function (result) {
                let products = JSON.parse(result)
                //Reset Table
                emptyTable("product-table");
                for (let product in products) {
                    productRow("product-table", products[product], product);
                    let updates = document.getElementsByClassName("update")
                    let dels = document.getElementsByClassName("delete")
                
                    updates[product].setAttribute('data-bs-toggle', 'modal');
                    updates[product].setAttribute('data-bs-target', '#updateModal');

                    updates[product].addEventListener('click', function () {
                        fillUpdateModal(products[product]._id.$oid)
                        }
                    );

                    dels[product].addEventListener('click', function () {
                        deleteProduct(products[product]._id.$oid)
                        }
                    );
                }
            })
        })
    }

     /**
     * Get Product value
     * 
     * @return {Null} 
     */
    function product_cms() {
        getProducts().done(function (result) {
            let products = JSON.parse(result)

            for (let product in products) {
                productRow("product-table", products[product], product);
                let updates = document.getElementsByClassName("update")
                let dels = document.getElementsByClassName("delete")
            
                updates[product].setAttribute('data-bs-toggle', 'modal');
                updates[product].setAttribute('data-bs-target', '#updateModal');

                updates[product].addEventListener('click', function () {
                    fillUpdateModal(products[product]._id.$oid)
                    }
                );

                dels[product].addEventListener('click', function () {
                    deleteProduct(products[product]._id.$oid)
                    }
                );
            }
        })

        let addBtn = document.getElementById('addBtn');
        let updateBtn = document.getElementById('updateBtn');


        addBtn.addEventListener('click', function () {
            //Take the element instead of the value directly to erase the field dynamically at the end of the function.
            let product_data = {};
            let inputElements = {
                name: document.getElementById('p-name'),
                image: document.getElementById('p-image'),
                price: document.getElementById('p-price')
            }
            //getting key by the value
            for (let element in inputElements) {
                product_data[getKeyByValue(inputElements, inputElements[element])] = inputElements[element].value
            }

            let isEmpty = false;

            Object.keys(product_data).forEach(function(key){
                if (product_data[key] == "") {
                    console.log("something is empty")
                    isEmpty = true
                }
            })
            if (!isEmpty) {
                product_data.price = parseInt(product_data.price)
                let tagsEl = document.getElementById('p-tags').value.split(',');
                let tags = [];
                for (let tag in tagsEl) {
                    tags.push(tagsEl[tag]);
                }

               product_data.tags = tags;
                //Add a Product
                addProduct(product_data).done(function (result) {
                    console.log(result)
                    //ReQuery new product
                    getProducts().done(function (result) {
                        let products = JSON.parse(result)
                        //Reset Table
                        emptyTable("product-table");
                        for (let product in products) {
                            productRow("product-table", products[product], product);
                            let updates = document.getElementsByClassName("update")
                            let dels = document.getElementsByClassName("delete")
                        
                            updates[product].setAttribute('data-bs-toggle', 'modal');
                            updates[product].setAttribute('data-bs-target', '#updateModal');
            
                            updates[product].addEventListener('click', function () {
                                fillUpdateModal(products[product]._id.$oid)
                                }
                            );
            
                            dels[product].addEventListener('click', function () {
                                deleteProduct(products[product]._id.$oid)
                                }
                            );
                        }
                    })

                })

                //Erasing the inputs
                for (let element in inputElements) {
                    inputElements[element].value = ""
                }
                document.getElementById('p-tags').value = ""
            }
        })

        updateBtn.addEventListener('click', function () {
            let date = new Date();

            let product_data = {};
            let inputElements = {
                id: document.getElementById('update_id'),
                name: document.getElementById('update_name'),
                price: document.getElementById('update_price'),
                image: document.getElementById('update_image')
            }

            //getting key by the value
            for (let element in inputElements) {
                product_data[getKeyByValue(inputElements, inputElements[element])] = inputElements[element].value
            }
            product_data['date'] = date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate()

            
            product_data.price = parseInt(product_data.price)
            let tagsEl = document.getElementById('update_tags').value.split(',');
            let tags = [];
            for (let tag in tagsEl) {
                tags.push(tagsEl[tag]);
            }

            product_data.tags = tags;
            //Update a Product
            updateProduct(product_data).done(function (result) {
                console.log(JSON.parse(result))
                //ReQuery new product
                getProducts().done(function (result) {
                    let products = JSON.parse(result)
                    //Reset Table
                    emptyTable("product-table");
                    for (let product in products) {
                        productRow("product-table", products[product], product);
                        let updates = document.getElementsByClassName("update")
                        let dels = document.getElementsByClassName("delete")
                    
                        updates[product].setAttribute('data-bs-toggle', 'modal');
                        updates[product].setAttribute('data-bs-target', '#updateModal');
        
                        updates[product].addEventListener('click', function () {
                            fillUpdateModal(products[product]._id.$oid)
                            }
                        );
        
                        dels[product].addEventListener('click', function () {
                            deleteProduct(products[product]._id.$oid)
                            }
                        );
                    }
                })

            })

            //Erasing the inputs
            for (let element in inputElements) {
                inputElements[element].value = ""
            }
            document.getElementById('update_tags').value = ""
        })
    }

    product_cms();
})