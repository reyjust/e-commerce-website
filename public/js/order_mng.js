import { orderMngRow } from "./table_Utilities/orderMngRow.js";
import { emptyTable } from "./table_Utilities/emptyTable.js";
import { getKeyByValue } from "./getKeyByValue.js";
  

document.addEventListener('DOMContentLoaded', (event) => {

    /**
        * Get data for Orders
        *
        * @return ajax method
    */
    function getOrders() {
        return $.ajax({
            url:"queries/order-cms-queries.php",    //the page containing php script
            type: "post",    //request type,
            // dataType: 'json',
            data: {action: "getOrders"}
        })
    }

    /**
        * Delete orders
        *
        * @param {Integer} $inputId
        *
        * @return ajax method
    */

    function delOrder(inputId) {
        return $.ajax({
            url:"queries/order-cms-queries.php",    //the page containing php script
            type: "post",    //request type,
            // dataType: 'json',
            data: {action: "delOrder", orderId: inputId}
        })
    }

     /**
        * Delete data from orders Id
        *
        * @param {Integer} $oId
        *
        * @return Null
    */
    function deleteOrder(oId) {
        delOrder(oId).done(function (result) {
            console.log(result)
            //ReQuery new orders
            getOrders().done(function (result) {
                let orders = JSON.parse(result)
                //Reset Table
                emptyTable("order-table");
                for (let order in orders) {
                    orderMngRow("order-table", orders[order], order);
        
                    let dels = document.getElementsByClassName("delete")

                    dels[order].addEventListener('click', function () {
                        deleteOrder(orders[order].id)
                        }
                    );
                }
            })
        })
    }
    
     /**
        * Generate order for CMS
        *
        * @return Null
    */
    function order_cms() {
        getOrders().done(function (result) {
            let orders = JSON.parse(result);
            console.log(orders)
            
            for (let order in orders) {
                orderMngRow("order-table", orders[order], order);

                let dels = document.getElementsByClassName("delete")

                dels[order].addEventListener('click', function () {
                    deleteOrder(orders[order].id)
                    }
                );
            }
        })

        // let delBtn = document.getElementById('delBtn');

        // delBtn.addEventListener('click', function () {
        //     let inputId = document.getElementById('inputId').value;

        //     if (inputId != "") {
        //         delOrder(inputId).done(function (result) {
        //             console.log(result)
        //             //ReQuery new orders
        //             getOrders().done(function (result) {
        //                 let orders = JSON.parse(result);
                          
        //                 emptyTable("order-table");
        //                 for (let order in orders) {
        //                     orderMngRow("order-table", orders[order], order);
        //                 }
        //             })

        //         })
        //         document.getElementById('inputId').value = "";
        //     } else {
        //         console.log("no Input")
        //     }
        // })
    }
    order_cms();
})