import { formStatus } from "./formStatus.js";


document.addEventListener('DOMContentLoaded', (event) => {
  let inputs = document.getElementsByTagName("input");

  Array.from(inputs).forEach((input, index) => {
    input.addEventListener('click', formStatus)
  })

  document.getElementById('submit').addEventListener('click', signup);
})


  /**
  * Return a JSON key by its value
  * 
  * @param {String} user_data JSON to search from
  * 
  * @return {ajax} ajax promise from registerUser form
  */
function registerUser(user_data) {
  return $.ajax({
      url:"queries/user-state-queries.php",    //the page containing php script
      type: "post",    //request type,
      // dataType: 'json',
      data: {action: "registerUser", user: user_data}
  })
}

function signup() {
  /**
    *Trigger on Sign Up Button Click. Create A user with form verification.
  */
  //Array of Users.
  let users;

  //Clear the validation errors.
  formStatus()

  let date = new Date();

  
  //Retrieve the values from Inputs.
  let user_data = {
    username: document.getElementById('username').value,
    name: document.getElementById('name').value,
    surname: document.getElementById('surname').value,
    email: document.getElementById('email').value,
    password: document.getElementById('password').value,
    phone_number: document.getElementById('phone_number').value,
    date: date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate(),
    gender: ""
  }

    let male = document.getElementById('male');
    let female = document.getElementById('female');

    if (male.checked) {
      user_data.gender = "male"
    } else if (female.checked) {
      user_data.gender = "female"
    }

    //Variables for validation.
    let result = { type: "", value: "" };
    let emptyValue = false;
    let badEmail = false;
    let emailTaken = false;

    // Checking all fields are filled.
    for (let data in user_data) {
      if (user_data[data] === "") {
        emptyValue = true
      }
    }
    //Checking email is in proper format.
    const specialChar = /[ `!#$%^&*()_+\-=\[\]{};':"\\|,<>\/?~]/;
    if ((!user_data.email.includes("@")) || (user_data.email.indexOf("@") == 0) || (user_data.email.indexOf("@") >= user_data.email.length - 5) || (!user_data.email.includes(".")) || (specialChar.test(user_data.email))) {
      badEmail = true;
    }

    //If not filled correctly
    if (!emptyValue) {
      //If email is not correct
      if (!badEmail) {
          //If password is longer than 8 char
          if (user_data.password.length >= 8) {

            registerUser(user_data).done(function (result) {
              let register = JSON.parse(result)
              emailTaken = register          
              if (emailTaken) {
                document.getElementById('submit').insertAdjacentHTML('beforebegin',
                '<p id="form-message">Success. Redirecting</p>');
                setTimeout(() => {
                  window.location.href = "http://" + window.location.hostname + "/login.php";
                }, 1000);
                
              } else {
                document.getElementById('submit').insertAdjacentHTML('beforebegin',
                '<p id="form-message">Email Taken</p>');
              }
            })

          } else {
            result.type = "Password must be 8 character long minimum."
          }
      } else {
        result.type = "Email is not correct."
      }
    } else {
      result.type = "Please fill all fields."
    }

    document.getElementById('submit').insertAdjacentHTML('beforebegin',
      '<p id="form-message">' + result.type + '</p>');
}

