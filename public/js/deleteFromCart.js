/**
        * Delete data from Cart
        *
        * @param Integer $productId
        *
        * @return basket New basket from localStorage
    */

export function deleteFromCart(productId) {
  let basket;
    if (typeof(Storage) !== "undefined") {

        basket = localStorage.getItem('Basket');
        basket = JSON.parse(basket);

        if (basket === null) {            
            basket = [];
            localStorage.setItem('Basket', JSON.stringify(basket));
        }

        let indexToDel = basket.indexOf(productId);
        basket.splice(indexToDel, 1);
    

      } else {
        // Sorry! No Web Storage support..
      }
    return basket
}
