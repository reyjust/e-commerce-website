import { deleteFromCart } from "./deleteFromCart.js";
import { changeCartQty } from "./changeCartQty.js";
import { emptyTable } from "./table_Utilities/emptyTable.js";
import { productCard } from "./product-card.js";
import { addToCart } from "./addToCart.js";

document.addEventListener('DOMContentLoaded', (event) => {
  function createOrder(basket) {
    return $.ajax({
        url:"queries/cart-queries.php",    //the page containing php script
        type: "post",    //request type,
        // dataType: 'json',
        data: {action: "createOrder", basket: basket}
    })
  }

  function cart() {
    let basket = localStorage.getItem('Basket')
    basket = JSON.parse(basket);

      if (basket.length == 0) {
        let body = document.getElementById("custom-table");
        body.insertAdjacentHTML("afterend","<p>Your Cart is Empty!</p>");
        //Disabled button
        let checkout = document.getElementById("checkout");
        checkout.classList.add("disabled");
        checkout.disabled = true;
      }
      
      for (let item in basket) {
        let container = document.getElementById("product-cart-table");
        // cartRow("product-cart-table", basket[item], item);

        let product = basket[item];

        let row = document.createElement("tr");
        row.classList.add("cart-item");
        if ((item%2) == 0) {
            row.classList.add("row-light");
        } else {
            row.classList.add("row-dark");
        }


        let name = document.createElement("td");
        name.textContent = product.name;
        let quantity = document.createElement("td");
        let unit =  document.createElement("p");
        unit.textContent = product.qty;

        let plus =  document.createElement("button");
        let minus =  document.createElement("button");

        plus.classList.add("btn", "btn-sm", "btn-outline-dark")
        minus.classList.add("btn", "btn-sm", "btn-outline-dark")

        plus.textContent = "+";
        minus.textContent = "-";

        plus.addEventListener('click',function () {
          let cartItems = document.getElementsByClassName("cart-item");
          let newBasket = changeCartQty(true, product.id, basket);

          localStorage.setItem('Basket', JSON.stringify(newBasket));


          for (let item in newBasket) {
            if (newBasket[item].id == product.id) {
              let qty = parseInt(cartItems[item].childNodes[1].childNodes[1].childNodes[0].data);

              cartItems[item].childNodes[1].childNodes[1].childNodes[0].data = qty+1;
              
              break
            }
          }
        });

        minus.addEventListener('click',function () {
          let cartItems = document.getElementsByClassName("cart-item");
          let newBasket = changeCartQty(false, product.id, basket);

          localStorage.setItem('Basket', JSON.stringify(newBasket));

          for (let item in newBasket) {
            if (newBasket[item].id == product.id) {
              let qty = parseInt(cartItems[item].childNodes[1].childNodes[1].childNodes[0].data);

              if (qty > 1) {
                cartItems[item].childNodes[1].childNodes[1].childNodes[0].data = qty-1;
              }
              break
            }
          }

        });

        let price = document.createElement("td");
        price.textContent = "RS 100";


    
        row.appendChild(name);

        quantity.appendChild(plus);
        quantity.appendChild(unit);
        quantity.appendChild(minus);

        row.appendChild(quantity);
        row.appendChild(price);

        let del = document.createElement("td");
        let delBtn = document.createElement("button");
        delBtn.classList.add("btn", "btn-sm", "btn-outline-dark")
        delBtn.textContent = "Delete";
        delBtn.addEventListener('click',function () {
          let cartItems = document.getElementsByClassName("cart-item");

  

          let newBasket = deleteFromCart(product.id);
          localStorage.setItem('Basket', JSON.stringify(newBasket));
          
          for (let item in newBasket) {
            console.log(newBasket[item].id +"\t"+ product.id)
            if (newBasket[item].id == product.id) {
              cartItems[item].remove();

              emptyTable('product-cart-table');
              cart();
              break
            }
          }

        });

        del.appendChild(delBtn)
        row.appendChild(del);


        container.appendChild(row);
      }

      document.getElementById("checkout").addEventListener('click', function() {
          let basket = localStorage.getItem('Basket')
          basket = JSON.parse(basket)
          createOrder(basket).done(function (result) {
            let response = JSON.parse(result);
            console.log(response);
            let emptyBasket = [];
            emptyTable('product-cart-table');
            localStorage.setItem('Basket', JSON.stringify(emptyBasket));

            let modalbox = document.getElementById("modalText");
            document.getElementById("checkout").disabled = true;
            modalbox.innerHTML =
             "Your Order is on its way<br >Thank you for choosing SockStar.com!<br> Your orderId is: " + response.newOrder + "<br><br><b> Because you like " + response.recommendations[0] + " products:</b>";
            // let modalRecommend = document.getElementById("recommends");
            for (let product in response.recommendations[1]) {
              modalbox.insertAdjacentHTML('beforeend', productCard(response.recommendations[1][product]));
              
              let btns = document.getElementsByClassName("addToCart");
              
              btns[product].addEventListener('click', function () {
                  addToCart(response.recommendations[1][product])
              })

            }
          })
      })
      document.getElementById("closeCheckout").addEventListener('click', function () {
          console.log("redirecting");
          setTimeout(() => {
            window.location.href = "http://" + window.location.hostname + "/index.php";
          }, 1000);
      })

    }
    cart();
})