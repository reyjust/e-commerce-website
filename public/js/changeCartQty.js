export function changeCartQty(addition, productId, basket) {

    for (let item in basket) {
        if (basket[item].id == productId) {
            if (addition) {
                basket[item].qty += 1;
            } else {
                if (basket[item].qty > 1) {
                    basket[item].qty -= 1;
                }
            }
          break
        }
      }

    return basket;
}