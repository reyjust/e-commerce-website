import { addToCart } from "./addToCart.js";


document.addEventListener('DOMContentLoaded', (event) => {


    let loadMoreBtn = document.getElementById("loadMore");
    loadMoreBtn.addEventListener('click', loadMore)

    let search = document.getElementById("searchProduct");
    let filter = document.getElementById("searchFilter");
    let order = document.getElementById("searchOrder");

    search.addEventListener('keyup', function () {
        research(search.value, filter.value, order.value).done(function (result) {
            let products = JSON.parse(result)
        
            document.getElementById('shop').innerHTML = "";

            for (let product in products) {
                let container = document.getElementById('shop');

                product = products[product];

                let card = document.createElement("div");
                card.classList.add("col-lg-2", "product-card");

                let img = document.createElement("img");
                img.classList.add("product-image");

                $.get('assets/img/' + product.image)
                .done(function() { 
                    img.src = 'assets/img/' + product.image;
            
                }).fail(function() {
                    img.src = 'assets/img/placeholder.png';
            
                })

                let div = document.createElement("div");
                div.classList.add("product-text");

                let name = document.createElement("p");
                name.textContent = product.name;
                let price = document.createElement("p");
                price.textContent = 'RS ' + product.price;
                price.style.color = '#FF4747';

                let btn = document.createElement("button");
                btn.classList.add("product-btn", "addToCart");
                btn.textContent = "Add to Cart";

                btn.addEventListener('click', function () {
                    addToCart(product)
                })

                card.appendChild(img);

                div.appendChild(name);
                div.appendChild(price);
                
                card.appendChild(div);
                card.appendChild(btn);
                container.appendChild(card);
            }

    })
        
    })

    /**
     * Return a JSON key by its value
     * 
     * @param {String} queryString  JSON to search from
     * @param {String} filter JSON to search from
     * @param {String} order JSON to search from
     * 
     * @return {ajax} ajax promise from research form
     */
    function research(queryString, filter = null, order = null) {
        return $.ajax({
            url:"queries/shop-queries.php",    //the page containing php script
            type: "post",    //request type,
            // dataType: 'json',
            data: {action: "research", queryString: queryString, filter: filter, order: order}
        })
    }


    
    /**
     * Return a JSON key by its value
     * 
     * @param {String} shop JSON to search from
     * 
     * @return {ajax} ajax promise from shop form
     */
    function shop() {
        $.ajax({
            url:"queries/shop-queries.php",    //the page containing php script
            type: "post",    //request type,
            // dataType: 'json',
            data: {action: "getShop"},
            success: function(result){
                let products = JSON.parse(result)
                console.log(products)
                for (let product in products) {
                    let container = document.getElementById('shop');
        
                    product = products[product];
        
                    let card = document.createElement("div");
                    card.classList.add("col-lg-2", "product-card");
        
                    let img = document.createElement("img");
                    img.classList.add("product-image");
                    $.get('assets/img/' + product.image)
                    .done(function() { 
                        img.src = 'assets/img/' + product.image;
                
                    }).fail(function() {
                        img.src = 'assets/img/placeholder.png';
                
                    })
        
                    let div = document.createElement("div");
                    div.classList.add("product-text");
        
                    let name = document.createElement("p");
                    name.textContent = product.name;
                    let price = document.createElement("p");
                    price.textContent = 'RS ' + product.price;
                    price.style.color = '#FF4747';
        
                    let btn = document.createElement("button");
                    btn.classList.add("product-btn", "addToCart");
                    btn.textContent = "Add to Cart";
        
                    btn.addEventListener('click', function () {
                        addToCart(product)
                    })
        
                    card.appendChild(img);
        
                    div.appendChild(name);
                    div.appendChild(price);
                    
                    card.appendChild(div);
                    card.appendChild(btn);
                    container.appendChild(card);
                }
            }
        });
    }


    
    /**
     * Return a JSON key by its value
     *
     * @return {ajax} ajax promise from shop form
     */
    function loadMore() {
        $.ajax({
            url:"queries/shop-queries.php",    //the page containing php script
            type: "post",    //request type,
            // dataType: 'json',
            data: {action: "getShop"},
            success: function(result){
                let products = JSON.parse(result)
                console.log(products)
                //Redo DB call here
                for (let product in products) {
                    let container = document.getElementById('shop');

                    product = products[product];

                    let card = document.createElement("div");
                    card.classList.add("col-lg-2", "product-card");

                    let img = document.createElement("img");
                    img.classList.add("product-image");
                    img.src = 'assets/img/' + product.image;

                    let div = document.createElement("div");
                    div.classList.add("product-text");

                    let name = document.createElement("p");
                    name.textContent = product.name;
                    let price = document.createElement("p");
                    price.textContent = product.price;
                    price.style.color = '#FF4747';

                    let btn = document.createElement("button");
                    btn.classList.add("product-btn", "addToCart");
                    btn.textContent = "Add to Cart";

                    btn.addEventListener('click', function () {
                        addToCart(product.id)
                    })

                    card.appendChild(img);

                    div.appendChild(name);
                    div.appendChild(price);
                    
                    card.appendChild(div);
                    card.appendChild(btn);
                    container.appendChild(card);
                }
            }
        });
    }

    shop();
})
