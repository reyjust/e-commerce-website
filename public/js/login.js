import { userStatus } from "./changeUserStatus.js";
import { formStatus } from "./formStatus.js";

document.addEventListener('DOMContentLoaded', (event) => {
  let inputs = document.getElementsByTagName("input");

  Array.from(inputs).forEach((input, index) => {
    input.addEventListener('click', formStatus)
  })

  document.getElementById('submit').addEventListener('click', login);
})


/**
 * Return a JSON key by its value
 * 
 * @param {String} login_data input from login
 * 
 * @return {ajax} ajax promise from login form
 */

function logInUser(login_data) {
  return $.ajax({
      url:"queries/user-state-queries.php",    //the page containing php script
      type: "post",    //request type,
      // dataType: 'json',
      data: {action: "logInUser", user: login_data}
  })
}


/**
 * Return a JSON key by its value
 * 
 * @return Null
 */
function login() {
  /**
    *Trigger on Log in button. Log in the User
  */
  formStatus()

  let login_data = {
    email: document.getElementById('email').value,
    password: document.getElementById('password').value,
  }

  let emptyValue = false;
  let result = { type: "", value: "" };

  // Checking all fields are filled.
  for (let data in login_data) {
    if (login_data[data] == "") {
      emptyValue = true
    }
  }

    if (!emptyValue) {
      logInUser(login_data).done(function (result) {
        let login = JSON.parse(result)
        console.log(result)
        if (typeof(login) == "object") {
          document.getElementById('submit').insertAdjacentHTML('beforebegin',
        '<p id="form-message">Success. Redirecting...</p>');
        setTimeout(() => {
          window.location.href = "http://" + window.location.hostname + "/";
        }, 1000);
        } else {
          document.getElementById('submit').insertAdjacentHTML('beforebegin',
          '<p id="form-message">' + result + '</p>');
        }
      })
    } else {
      result.type = "Please fill all fields."
    }

    document.getElementById('submit').insertAdjacentHTML('beforebegin',
      '<p id="form-message">' + result.type + '</p>');
}