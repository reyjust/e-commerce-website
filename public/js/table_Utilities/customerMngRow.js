export function customerMngRow(container, customer, loopNo) {
    container = document.getElementById(container);

    let row;
    if ((loopNo%2) == 0) {
        row = "<tr class='row-light'>"
    } else {
        row = "<tr class='row-dark'>"
    }
    row += 
        "<td>" + customer.id + "</td>\
         <td>" + customer.email + "</td>\
         <td>" + customer.order_qty + "</td>\
         <td>" + customer.date + "</td>\
         <td><button class='update'><i class='text-center fas fa-pen fa-1x'></i></button><button class='delete'><i class='text-center fas fa-trash fa-1x'></i></button></td>\
         <td></td>\
    </tr>"
    container.insertAdjacentHTML('beforeend',row);
}