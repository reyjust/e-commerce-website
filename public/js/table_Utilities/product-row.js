export function productRow(container, product, color) {
    container = document.getElementById(container);
    let row;
    if ((color%2) == 0) {
        row = "<tr class='row-light'>"
    } else {
        row = "<tr class='row-dark'>"
    }
    row += 
        "<td>" + product._id.$oid + "</td>\
         <td>" + product.name + "</td>\
         <td>" + product.price + "</td>\
         <td>" + product.image + "</td>\
         <td><button class='update'><i class='text-center fas fa-pen fa-1x'></i></button><button class='delete'><i class='text-center fas fa-trash fa-1x'></i></button></td>\
         <td></td>\
    </tr>"
    container.insertAdjacentHTML('beforeend',row);
}