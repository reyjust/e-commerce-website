export function orderRow(container, order, color) {
    container = document.getElementById(container);
    let row;
    if ((color%2) == 0) {
        row = "<tr class='row-light'>"
    } else {
        row = "<tr class='row-dark'>"
    }
    row += 
        "<td>" + order.id + "</td>\
         <td>" + order.date + "</td>\
         <td><button class='receipts'><i class='text-center fas fa-receipt fa-1x'></i></button></td>\
    </tr>"
    container.insertAdjacentHTML('beforeend',row);
}