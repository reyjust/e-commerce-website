export function addToCart(product) {
    if (typeof(Storage) !== "undefined") {

        let basket = localStorage.getItem('Basket');
        basket = JSON.parse(basket);
        if (basket === null) {            
            basket = [];
            localStorage.setItem('Basket', JSON.stringify(basket));
        }


        let isExisting = false;

        for (let item in basket) {
          if (basket[item].id == product._id.$oid) {
            isExisting = true;
            basket[item].qty += 1
            break
          }
        }

        if ( !isExisting ) {
          let newProduct = {
            id: product._id.$oid,
            name: product.name,
            price: product.price,
            qty: 1
          }
          basket.push(newProduct);
        }
    
        localStorage.setItem('Basket', JSON.stringify(basket));

      } else {
        // Sorry! No Web Storage support..
      }

}
