<?php

require_once(dirname(dirname(__FILE__)).'/modules/connectDb.php');

function getOrders() {
    $mongo = connectDb();
    $db = $mongo[1];
    
    $collection = $db->orders;
    $cursor = $collection->find();

    $orders = array();
    foreach($cursor as $o)
    {
        $orderData = array( "id" => $o->_id.$oid, "customerId"=> $o->customerId.$oid,  "paid" => $o->paid, "date" => $o->date);
        array_push($orders, $orderData);
    }

    return $orders;
}


function delOrder($orderId) {
    $mongo = connectDb();
    $db = $mongo[1];
    
    $collection = $db->orders;
    $cursor = $collection->deleteOne( array( "_id" => new MongoDB\BSON\ObjectID($orderId)) );
    $deletedCount = $cursor->getDeletedCount();

    if ($deletedCount != 0) {
        return true;
    }

    return false;
}

if (isset($_POST['action'])) {
    switch ($_POST['action']) {
        case 'getOrders':
            $orders = getOrders();
            echo json_encode($orders);
            break;
        case 'delOrder':
            $delOrderId = $_POST['orderId'];

            $del = delOrder($delOrderId);
            if ($del) {
                echo json_encode("Deleted!");
            } else {
                echo json_encode("Wrong Id");
            }

            break;
    }
    exit;
}
?>