<?php

require_once(dirname(dirname(__FILE__)).'/modules/connectDb.php');
require_once(dirname(dirname(__FILE__)).'/modules/getLoggedUser.php');

function getUser($loggedUser) {
    $mongo = connectDb();
    $db = $mongo[1];

    $collection = $db->customers;
    $cursor = $collection->findOne( array("email" => $loggedUser['logged_email'] ) );

    return $cursor;
}
function updateProfile($profileData) {
    $mongo = connectDb();
    $db = $mongo[1];
    
    $collection = $db->customers;

    $id = $profileData["id"];
    unset($profileData["id"]);

    $profileData["role"] = $loggedUser['logged_role'];

    $updateResult = $collection->updateOne(
        [ '_id' => new MongoDB\BSON\ObjectID($id) ],
        [ '$set' => $profileData]
     );

    if ($updateResult->getMatchedCount() == $updateResult->getModifiedCount()) {
        return true;
    } else {
        return false;
    }
}

function getUserOrders($loggedUser) {
    $mongo = connectDb();
    $db = $mongo[1];
    
    $collection = $db->orders;
    $cursor = $collection->find( array("customerId" => new MongoDB\BSON\ObjectID($loggedUser['logged_id']) ) );

    $orders = array();
    foreach($cursor as $o)
    {
        $orderData = array( 
            "id"=> $o->_id.$oid,
            "date" => $o->date,
        );
        array_push($orders, $orderData);
    }

    return $orders;

}

function getReceipt($oid) {
    $mongo = connectDb();
    $db = $mongo[1];
    
    $collection = $db->orderDetails;
    $cursor = $collection->find( array("orderId" => new MongoDB\BSON\ObjectID($oid) ) );
    $cursor = $cursor->toArray();


    return $cursor;
}

if (isset($_POST['action'])) {
    switch ($_POST['action']) {
        case 'getAccount':
            $loggedUser = getLoggedUser();
            $info = getUser($loggedUser);
            $orders = getUserOrders($loggedUser);
            
            $account = array('info' => $info, 'orders' => $orders);
            echo json_encode($account);
            break;
        case 'updateProfile':
            $newProfile = $_POST['profile'];

            $updateProfile = updateProfile($newProfile);

            if ($updateProfile) {
                $loggedUser = getLoggedUser();
                $info = getUser($loggedUser);

                echo json_encode($info);
            } else {
                echo json_encode("error");
            }

            break;
        case 'getReceipt':
            $orderId = $_POST['order'];

            $receipt = getReceipt($orderId);

            echo json_encode($receipt);
            
            break;

    }
}
?>