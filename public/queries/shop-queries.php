<?php

require_once(dirname(dirname(__FILE__)).'/modules/connectDb.php');

function getShop() {
    $mongo = connectDb();
    $db = $mongo[1];
    
    $collection = $db->products;
    $cursor = $collection->find();
    $cursor = $cursor->toArray();

    return $cursor;
}

function research($queryString, $filter, $order) {
    $mongo = connectDb();
    $db = $mongo[1];
    
    $collection = $db->products;
    $cursor = $collection->find(
        array(
            "name" => new \MongoDB\BSON\Regex($queryString)
        ),
        array(
            "sort" =>
                array(
                    $filter => intval($order)
                )
        )
    );
    $cursor = $cursor->toArray();

    return $cursor;
}

if (isset($_POST['action'])) {
    switch ($_POST['action']) {

        case 'getShop':
            $res = getShop();
            echo json_encode($res);
            break;

        case 'research':
            $queryString = $_POST['queryString'];
            $filter = $_POST['filter'];
            $order = $_POST['order'];

            $result = research($queryString, $filter, $order);
            echo json_encode($result);
            break;
    }
    exit;
}
?>