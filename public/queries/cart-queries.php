<?php
require_once(dirname(dirname(__FILE__)).'/modules/connectDb.php');
require_once(dirname(dirname(__FILE__)).'/modules/getLoggedUser.php');
$user = getLoggedUser();

function countTags($products) {
    $tagCounter = array();

    foreach( $products as $tags) {
        foreach( $tags as $tag) {
            if ( !array_key_exists($tag, $tagCounter) ) {
                $tagCounter[$tag] = 1;
            } else {
                $tagCounter[$tag] += 1;
            }
        }
    }
    return $tagCounter;
}

function createOrder($basket, $user) {
    $mongo = connectDb();
    $db = $mongo[1];
    
    $order = $db->orders;

    

    $cursor = $order->insertOne(
        array(
            'customerId' => $user['logged_id'],
            'date' => date("Y/m/d")
        )
    );
    $newId = $cursor->getInsertedId();
    $newId = $newId.$oid;

    $collection = $db->orderDetails;


    foreach ($basket as $item) {
        $cursor = $collection->insertOne(
            array(
            'orderId' => new MongoDB\BSON\ObjectID($newId),
            'customerId' => $user['logged_id'],
            'productId' => $item['id'],
            'productName' => $item['name'],
            'qty' => $item['qty'],
            'paid' => strval(intval($item['price'])*intval($item['qty']))
            )
        );
    }
    return $newId;
}
function getRecommendations($ids) {
    $mongo = connectDb();
    $db = $mongo[1];
    
    $collection = $db->products;
    $mongoId = array();
    foreach ($ids as $id) {
        array_push($mongoId, new MongoDB\BSON\ObjectID($id));
    }

    $cursor = $collection->find( array("_id" => array( '$in' => $mongoId) ) );
    $cursor = $cursor->toArray();

    $tags = array();
    foreach ($cursor as $item) {
        array_push($tags, $item['tags']);
    }
    $tagsFrequency = countTags($tags);
    $bestTags = array_keys($tagsFrequency);

    $cursor = $collection->find( array("tags" => $bestTags[0] ) );
    $cursor = $cursor->toArray();
    
    return array($bestTags[0], $cursor);
}

if (isset($_POST['action'])) {
    switch ($_POST['action']) {
        case 'createOrder':
            $basket = $_POST['basket'];

            $newOrder = createOrder($basket, $user);

            $ids = array();
            foreach ($basket as $id) {
                array_push($ids, $id['id']);
            }
            $recommendations = getRecommendations($ids);

            echo json_encode(array("newOrder" => $newOrder, "recommendations" => $recommendations));
            
            break;
    }
}
?>