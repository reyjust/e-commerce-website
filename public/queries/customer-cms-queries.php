<?php

require_once(dirname(dirname(__FILE__)).'/modules/connectDb.php');

function getCustomers() {
    $mongo = connectDb();
    $db = $mongo[1];
    
    $collection = $db->customers;
    $cursor = $collection->find();

    $orderCollection = $db->orders;

    $customers = array();
    foreach($cursor as $c)
    {        

        $cursor_orders = $orderCollection->count( array( "customerId" => new MongoDB\BSON\ObjectID($c->_id.$oid) ) );

        $customerData = array( 
            "id" => $c->_id.$oid,
            "email" => $c->email,
            "date" => $c->date,
            "order_qty" => $cursor_orders
        );
        
        array_push($customers, $customerData);
    }

    return $customers;
}

function addCustomer($customer) {
    $mongo = connectDb();
    $db = $mongo[1];
    
    $collection = $db->customers;
    $cursor = $collection->findOne( array("email" => $customer['email']) );

    if ($cursor == null) {
        $cursor = $collection->insertOne($customer);

        return true;
    }

    return false;
}

function delCustomer($customerId) {
    $mongo = connectDb();
    $db = $mongo[1];
    
    $collection = $db->customers;
    $cursor = $collection->deleteOne( array( "_id" => new MongoDB\BSON\ObjectID($customerId)) );
    $deletedCount = $cursor->getDeletedCount();

    if ($deletedCount != 0) {
        return true;
    }

    return false;
}

function updateCustomer($userData) {
    $mongo = connectDb();
    $db = $mongo[1];
    
    $collection = $db->customers;

    $id = $userData["id"];
    unset($userData["id"]);

    $updateResult = $collection->updateOne(
        [ '_id' => new MongoDB\BSON\ObjectID($id) ],
        [ '$set' => $userData]
     );

    if ($updateResult->getMatchedCount() == $updateResult->getModifiedCount()) {
        return true;
    } else {
        return false;
    }
}

function getCustomerDataById($userId) {
    $mongo = connectDb();
    $db = $mongo[1];

    $collection = $db->customers;
    $cursor = $collection->findOne( array("_id" => new MongoDB\BSON\ObjectID($userId) ) );
    

    return $cursor;
    
}

if (isset($_POST['action'])) {
    switch ($_POST['action']) {
        case 'getCustomers':
            $customers = getCustomers();
            echo json_encode($customers);
            break;
        case 'addCustomer':
            $newUser = $_POST['user'];

            $add = addCustomer($newUser);
            if ($add) {
                echo json_encode("User Created");
            } else {
                echo json_encode("Email duplicate");
            }

            break;
        case 'delCustomer':
            $delCustomerId = $_POST['userId'];

            $del = delCustomer($delCustomerId);
            if ($del) {
                echo json_encode("Deleted!");
            } else {
                echo json_encode("Wrong Id");
            }

            break;
        
        case 'updateCustomer':
            $newData = $_POST['user'];

            $updateCustomer = updateCustomer($newData);

            if ($updateCustomer) {
                echo json_encode("Updated!");
            } else {
                echo json_encode("Error");
            }

            break;

        case 'getCustomerDataById':
            $userId = $_POST['userId'];

            $user_data = getCustomerDataById($userId);

            echo json_encode($user_data);
            break;
    }
    exit;
}
?>