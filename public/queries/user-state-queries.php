<?php
require_once(dirname(dirname(__FILE__)).'/modules/connectDb.php');

function registerUser($customer) {
    $mongo = connectDb();
    $db = $mongo[1];
    
    $collection = $db->customers;
    $cursor = $collection->findOne( array("email" => $customer['email']) );

    if ($cursor == null) {
        $customer['role'] = "customer";
        $cursor = $collection->insertOne($customer);

        return true;
    }

    return false;
}

function logInUser($userInput) {
    $mongo = connectDb();
    $db = $mongo[1];
    
    $collection = $db->customers;
    $cursor = $collection->findOne( array("email" => $userInput['email']) );

    if ($cursor != null) {
        if ($cursor['password'] == $userInput['password']) {
            session_start();
            $_SESSION["logged_email"] = $userInput['email'];
            $_SESSION["logged_password"] = $userInput['password'];

            return array("logged_email" => $_SESSION["logged_email"], "logged_password" => $_SESSION["logged_password"]);
        } else {
            return "password not good";
        }
    } else {
        return "email not good";
    }
}

function logOutUser() {
    session_unset();
    session_destroy();

    return true;
}

if (isset($_POST['action'])) {
    switch ($_POST['action']) {
        case 'registerUser':
            $userInput = $_POST['user'];
            $register = registerUser($userInput);
            echo json_encode($register);
            break;

        case 'logInUser':
            $userInput = $_POST['user'];
            $login = logInUser($userInput);

            echo json_encode($login);
            break;

        case 'logOutUser':    
            session_unset();
            session_destroy();
            // $logout = logOutUser();

            echo json_encode($logout);
            break;
    }
    exit;
}
?>