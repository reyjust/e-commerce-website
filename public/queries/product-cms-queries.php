<?php

require_once(dirname(dirname(__FILE__)).'/modules/connectDb.php');

function getProducts() {
    $mongo = connectDb();
    $db = $mongo[1];
    
    $collection = $db->products;
    $cursor = $collection->find();
    $cursor = $cursor->toArray();

    // $products = array();
    // foreach($cursor as $p)
    // {
    //     $productData = array( "id" => $p->_id.$oid, "name"=> $p->name, "price" => $p->price, "image" => $p->image);
    //     array_push($products, $productData);
    // }

    return $cursor;
}



function addProduct($product) {
    $mongo = connectDb();
    $db = $mongo[1];
    $product['price'] = intval($product['price']);

    $collection = $db->products;

    $cursor = $collection->insertOne($product);

    return true;
}

function delProduct($productId) {
    $mongo = connectDb();
    $db = $mongo[1];
    
    $collection = $db->products;
    $cursor = $collection->deleteOne( array( "_id" => new MongoDB\BSON\ObjectID($productId)) );
    $deletedCount = $cursor->getDeletedCount();

    if ($deletedCount != 0) {
        return true;
    }

    return false;
}
function updateProduct($productData) {
    $mongo = connectDb();
    $db = $mongo[1];

    $product['price'] = intval($product['price']);
    
    $collection = $db->products;

    $id = $productData["id"];
    unset($productData["id"]);

    $updateResult = $collection->updateOne(
        [ '_id' => new MongoDB\BSON\ObjectID($id) ],
        [ '$set' => $productData]
     );

    if ($updateResult->getMatchedCount() == $updateResult->getModifiedCount()) {
        return true;
    } else {
        return false;
    }
}

function getProductDataById($productId) {
    $mongo = connectDb();
    $db = $mongo[1];

    $collection = $db->products;
    $cursor = $collection->findOne( array("_id" => new MongoDB\BSON\ObjectID($productId) ) );
    

    return $cursor;
    
}

if (isset($_POST['action'])) {
    switch ($_POST['action']) {

        case 'getProducts':
            $products = getProducts();
            echo json_encode($products);
            break;

        case 'addProduct':
            $newProduct = $_POST['product'];

            $add = addProduct($newProduct);
            if ($add) {
                echo json_encode("Product Created");
            } else {
                echo json_encode("Error");
            }
            break;

        case 'delProduct':
            $delProductId = $_POST['productId'];

            $del = delProduct($delProductId);
            if ($del) {
                echo json_encode("Deleted!");
            } else {
                echo json_encode("Wrong Id");
            }
            break;

        case 'updateProduct':
            $newData = $_POST['product'];

            $updateProduct = updateProduct($newData);

            if ($updateProduct) {
                echo json_encode("Updated!");
            } else {
                echo json_encode("Error");
            }

            break;

        case 'getProductDataById':
            $productId = $_POST['productId'];

            $product_data = getProductDataById($productId);

            echo json_encode($product_data);
            break;

    }
    exit;
}
?>