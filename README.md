# E-Commerce Website #

This project is intended for CST2120 Web Applications and Databases Module along Middlesex University of Mauritius, Year 2, BsC Computer Science and System Engineering. 

### Summary ###

* About
* Folder Structure
* Prerequise & Deployment

### About ###

This project is a simple group work of a website this 9 pages, implemented using PHP, HTML, Javascript and CSS.
It try to recreate a real e-commerce website.It is based on selling the left sock. A bit funny, this project is not based on what we sell but how we sell it.

The website is constituted of 9 pages:

* Home
* Shop
* Cart
* My Account
* Log In
* Sign In
* Product Management (CMS only)
* Customer Management (CMS only)
* Order Management (CMS only)

PHP is used for, rendering the HTML, avoiding duplicates with function, writing/reading player information among the Database (MYSQL) and several other tasks.

For the aesthetic, Bootstrap as been used along with custom CSS.
These imports are in CDN format to make the file smaller, but you will internet connection to test it out.

### Folder Structure ###

All pages are place in the root folder.
This is an example of the folder structure, it does not contain all the files.
```bash
.
├── public
│   ├── account.php
│   ├── assets
│   │   └── img
│   │       ├── avatars
│   │       │   ├── guy.png
│   │       │   └── women.png
│   │       ├── placeholder.png
│   │       ├── sock_android.png
│   │       ├── sock_black.png
│   │       ├── sock_bundle.png
│   │       ├── sock_default.png
│   │       ├── sock_feet.png
│   │       ├── sock.ico
│   │       ├── sock_nike_bundle.png
│   │       ├── sock.png
│   │       ├── sock_rainbow.png
│   │       ├── sock_running.png
│   │       ├── sock_strawberry.png
│   │       ├── sock_traits.png
│   │       ├── sock_white.png
│   │       └── sock_zebra.png
│   ├── cart.php
│   ├── css
│   │   └── styles.css
│   ├── customer_mng.php
│   ├── index.php
│   ├── js
│   │   ├── account.js
│   │   ├── addToCart.js
│   │   ├── cart.js
│   │   ├── changeCartQty.js
│   │   ├── changeUserStatus.js
│   │   ├── customer_mng.js
│   │   ├── deleteFromCart.js
│   │   ├── formStatus.js
│   │   ├── isLoggedIn.js
│   │   ├── login.js
│   │   ├── order_mng.js
│   │   ├── product-card.js
│   │   ├── product_cms.js
│   │   ├── scripts.js
│   │   ├── shop.js
│   │   ├── signup.js
│   │   └── table_Utilities
│   │       ├── cart-row.js
│   │       ├── customerMngRow.js
│   │       ├── orderMngRow.js
│   │       ├── order-row.js
│   │       └── product-row.js
│   ├── login.php
│   ├── modules
│   │   ├── cmsFooter.php
│   │   ├── cmsNavigation.php
│   │   ├── divider.php
│   │   ├── footer.php
│   │   ├── head.php
│   │   ├── import.php
│   │   └── navigation.php
│   ├── order_mng.php
│   ├── product_mng.php
│   ├── shop.php
│   └── signup.php
└── README.md
```


### Prerequise & Deployment ###

For the good practice, simple run ```bash npm i```, even if no depencies are added to this project.

Don't forget to stop Xammp services before doing this manipulation.
You will need a Xammp environment and place the folder into the working directory. most of the time it will be var/www/.

Composer is also controlling some libraries for this project, so to install mongodb libraries for instance, you will need to run ```bash composer install```. 

### Database ###

A MongoDump is provided with the project.
You will need to add MongoDb driver to your php installation. you can see it inside ```php phpinfo();```


#### Steps
* Stop xampp services
* You can also copy the and place the project to the root folder manually.
* Change your server root dir to : `you_dir/e-commerce-website/public/`
* sudo npm i (even if all packages are CDN, just for good practise)
* Restart xampp services
* Browse http://localhost:80


### NOTES
* sudo /opt/lampp/manager-linux-x64.run for starting XAMPP GUI on 64x based systems.
Made by Justin, Farhaan and Florrie